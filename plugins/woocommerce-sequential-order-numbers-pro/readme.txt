=== WooCommerce Sequential Order Numbers Pro ===
Author: woothemes, skyverge
Tags: woocommerce
Requires at least: 4.0
Tested up to: 4.4.1
Requires WooCommerce at least: 2.3.6
Tested WooCommerce up to: 2.5.0

== Installation ==

1. Upload the entire 'woocommerce-sequential-order-numbers-pro' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
