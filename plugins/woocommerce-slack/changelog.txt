*** WooCommerce Slack Changelog ***

2016.01.20 - version 1.1.2
 * Ensures 'new order' notification only gets sent once
 * Fix issue with username encoding
 * Adds compatibility for sequential order numbers 

2015.07.29 - version 1.1.1
 * Added support to WooCommerce 2.4
 * Fixed and improved the plugin logger

2015.03.12 - version 1.1.0
 * Private Groups Support
 * Option for 'sender name'
 * Multiple channels/groups for notifications
 * Add .pot / .mo files for localization

2015.01.20 - version 1.0.0
 * First Release.
